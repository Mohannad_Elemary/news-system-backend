<?php

use App\Http\Controllers\V1\Articles\ArticleController;
use App\Http\Controllers\V1\Articles\AuthorController;
use App\Http\Controllers\V1\Articles\CategoryController;
use App\Http\Controllers\V1\Articles\SourceController;
use App\Http\Controllers\V1\Users\AuthController;
use App\Http\Controllers\V1\Users\UserSettingController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1/auth'], function () {
    Route::post('/register', [AuthController::class, 'register'])->name('register');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
});

Route::group(['prefix' => 'v1', 'middleware' => ['AddAuthHeader', 'auth:api']], function () {
    Route::get('articles/authors', [AuthorController::class, 'index'])->name('articles.authors');
    Route::get('articles/categories', [CategoryController::class, 'index'])->name('articles.categories');
    Route::get('articles/sources', [SourceController::class, 'index'])->name('articles.sources');
    Route::resource('articles', ArticleController::class)->only(['index', 'show']);
});

Route::group(['prefix' => 'v1/users', 'middleware' => ['AddAuthHeader', 'auth:api']], function () {
    Route::put('settings', [UserSettingController::class, 'updateForAuthUser']);
});
