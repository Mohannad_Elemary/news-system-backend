<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    const PROVIDER = 'users';

    public function prepareDatabase()
    {
        $this->artisan('migrate');
        $this->artisan('db:seed');
        $this->artisan('cache:clear');
    }

    protected function getPassportClient()
    {
        return $this->getClient(self::PROVIDER);
    }

    private function getClient($provider)
    {
        $this->artisan('passport:client', ['--password' => null, '--no-interaction' => true, '--provider' => $provider]);
        $this->artisan('passport:keys', ['--no-interaction' => true]);
        return DB::table('oauth_clients')->where(['password_client' => 1, 'provider' => $provider])->first();
    }

    protected function getLoggedInUserToken()
    {
        $client = $this->getPassportClient();
        $user = User::factory()->create(['password' => 'password']);

        $body = [
            'username' => $user->email,
            'password' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'grant_type' => 'password',
            'scope' => '*'
        ];

        $response =  $this->json('POST', '/oauth/token', $body, ['Accept' => 'application/json']);

        return $response->json()['access_token'];
    }
}
