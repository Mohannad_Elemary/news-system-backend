<?php

namespace Tests\Feature\Articles;

use App\Models\Article;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListArticlesTest extends TestCase
{
    const ROUTE_LIST = 'articles.index';
    public $mockConsoleOutput = false;

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareDatabase();
    }

    /** @test */
    public function will_list_all_articles()
    {
        Article::factory(5)->create();
        $token = $this->getLoggedInUserToken();

        $response = $this->json('GET', route(self::ROUTE_LIST), [], ['Authorization' => "Bearer $token"]);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertCount(5, $response->json()['data']);
    }

    /** @test */
    public function will_filter_with_keyword()
    {
        Article::factory(5)->create();
        Article::factory()->create(['description' => "test keyword search"]);

        $token = $this->getLoggedInUserToken();

        $response = $this->json('GET', route(self::ROUTE_LIST), ['keyword' => 'keyword'], ['Authorization' => "Bearer $token"]);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertCount(1, $response->json()['data']);
    }

    /** @test */
    public function will_fail_if_not_authorized()
    {
        Article::factory(5)->create();

        $response = $this->json('GET', route(self::ROUTE_LIST));

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
