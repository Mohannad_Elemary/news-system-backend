<?php

namespace Tests\Feature\Articles;

use App\Models\Author;
use Illuminate\Http\Response;
use Tests\TestCase;

class ListAuthorsTest extends TestCase
{
    const ROUTE_LIST = 'articles.authors';
    public $mockConsoleOutput = false;

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareDatabase();
    }

    /** @test */
    public function will_list_all_authors()
    {
        Author::factory(5)->create();
        $token = $this->getLoggedInUserToken();

        $response = $this->json('GET', route(self::ROUTE_LIST), [], ['Authorization' => "Bearer $token"]);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertCount(5, $response->json()['data']);
    }

    /** @test */
    public function will_fail_if_not_authorized()
    {
        Author::factory(5)->create();

        $response = $this->json('GET', route(self::ROUTE_LIST));

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
