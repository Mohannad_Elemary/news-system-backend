<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{
    const ROUTE_LOGIN = 'login';
    public $mockConsoleOutput = false;

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareDatabase();
    }

    /** @test */
    public function will_fail_with_validation_errors_when_email_is_missing()
    {
        $response = $this->json('POST', route(self::ROUTE_LOGIN), [
            'password' => 'password',
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'email'
            ]);
    }

    /** @test */
    public function will_fail_with_validation_errors_when_password_is_missing()
    {
        $response = $this->json('POST', route(self::ROUTE_LOGIN), [
            'email' => 'john.doe@email.com',
            'password' => '',
        ]);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'password'
            ]);
    }

    /** @test */
    public function user_can_login_with_correct_credentials()
    {
        $client = $this->getPassportClient();

        $user = User::factory()->create(['password' => 'password']);

        $response = $this->json('POST', route(self::ROUTE_LOGIN), [
            'email' => $user->email,
            'password' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret
        ]);

        $response->assertStatus(Response::HTTP_OK);

        $this->assertArrayHasKey('access_token', $response->json()['data']);
        $this->assertArrayHasKey('refresh_token', $response->json()['data']);
        $this->assertArrayHasKey('name', $response->json()['data']);
    }

    /** @test */
    public function user_cannot_login_with_incorrect_credentials()
    {
        $client = $this->getPassportClient();

        $customer = User::factory()->create(['password' => bcrypt('password')]);

        $response = $this->json('POST', route(self::ROUTE_LOGIN), [
            'email' => $customer->email,
            'password' => 'invalid-password',
            'client_id' => $client->id,
            'client_secret' => $client->secret
        ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
