<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use WithFaker;

    const ROUTE_REGISTER = 'register';
    const CUSTOMER_PASSWORD = 'PAssw0rd';

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareDatabase();
    }

    public $mockConsoleOutput = false;

    /** @test */
    public function will_register_successfully()
    {
        $client = $this->getPassportClient();
        $userData = $this->getUserData([
            'password' => self::CUSTOMER_PASSWORD, 'password_confirmation' => self::CUSTOMER_PASSWORD, 'client_id' => $client->id,
            'client_secret' => $client->secret
        ]);

        $response = $this->json('POST', route(self::ROUTE_REGISTER), $userData);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertArrayHasKey('data', $response->json());
        $this->assertDatabaseCount(app(User::class)->getTable(), 1);
        $this->assertArrayHasKey('name', $response->json()['data']);
    }

    /** @test */
    public function will_fail_with_validation_errors_when_password_is_missing_or_invalid()
    {
        $userData = $this->getUserData(['password' => '']);
        $response = $this->json('POST', route(self::ROUTE_REGISTER), $userData);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'password'
            ]);

        $userData = $this->getUserData(['password' => 'password', 'password_confirmation' => 'password']);
        $response = $this->json('POST', route(self::ROUTE_REGISTER), $userData);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'password'
            ]);

        $userData = $this->getUserData([
            'email' => 'Test1@test.com',
            'password' => 'Test1@test.com', 'password_confirmation' => 'Test1@test.com'
        ]);
        $response = $this->json('POST', route(self::ROUTE_REGISTER), $userData);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'password'
            ]);
    }

    /** @test */
    public function will_fail_with_validation_errors_when_email_exists()
    {
        $userData = $this->getUserData(['password' => self::CUSTOMER_PASSWORD, 'password_confirmation' => self::CUSTOMER_PASSWORD]);
        $response = $this->json('POST', route(self::ROUTE_REGISTER), $userData);

        $response = $this->json('POST', route(self::ROUTE_REGISTER), $userData);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'email'
            ]);
    }

    private function getUserData($data = [])
    {
        return User::factory()->raw($data);
    }
}
