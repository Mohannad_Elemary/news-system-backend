<?php

namespace Tests\Feature\Articles;

use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Services\Integrations\NewYorkNewsAPIService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class NewYorkNewsTest extends TestCase
{
    public $mockConsoleOutput = false;

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareDatabase();
    }

    /** @test */
    public function will_sync_article_successfully()
    {
        $this->fakeAPIResponse();

        app(NewYorkNewsAPIService::class)->sync();

        $this->assertEquals(1, Category::count());
        $this->assertEquals(1, Source::count());
        $this->assertEquals(1, Author::count());
        $this->assertEquals(1, Article::count());

        $this->assertEquals('Business Day', Category::first()->name);
        $this->assertEquals('The New York Times', Source::first()->name);
        $this->assertEquals('Benjamin Mullin and Katherine Rosman', Author::first()->name);
        $this->assertEquals('Vox Media Is Raising $100 Million From Penske Media', Article::first()->title);
    }

    private function fakeAPIResponse()
    {
        Http::fake([
            'https://api.nytimes.com/*' => Http::response([
                "status" => "OK",
                "copyright" => "Copyright (c) 2023 The New York Times Company. All Rights Reserved.",
                "response" => [
                    "docs" => [
                        [
                            "abstract" => "The deal is the latest in a series of acquisitions and investments made by Penske Media, the owner of a swath of entertainment and trade publications including Rolling Stone and Variety.",
                            "web_url" => "https://www.nytimes.com/2023/02/06/business/vox-penske-media.html",
                            "snippet" => "The deal is the latest in a series of acquisitions and investments made by Penske Media, the owner of a swath of entertainment and trade publications including Rolling Stone and Variety.",
                            "lead_paragraph" => "Vox Media, owner of publications including New York Magazine and tech site The Verge, is raising $100 million from Penske Media, the owner of a swath of entertainment and trade publications including Rolling Stone and Variety, according to several people with knowledge of the deal.",
                            "print_section" => "B",
                            "print_page" => "3",
                            "source" => "The New York Times",
                            "headline" => [
                                "main" => "Vox Media Is Raising $100 Million From Penske Media",
                                "kicker" => null,
                                "content_kicker" => null,
                                "print_headline" => "Vox Is Said to Get $100 Million Infusion",
                                "name" => null,
                                "seo" => null,
                                "sub" => null
                            ],
                            "pub_date" => "2023-02-06T21:30:06+0000",
                            "document_type" => "article",
                            "news_desk" => "Business",
                            "section_name" => "Business Day",
                            "byline" => [
                                "original" => "By Benjamin Mullin and Katherine Rosman",
                                "person" => [
                                    [
                                        "firstname" => "Benjamin",
                                        "middlename" => null,
                                        "lastname" => "Mullin",
                                        "qualifier" => null,
                                        "title" => null,
                                        "role" => "reported",
                                        "organization" => "",
                                        "rank" => 1
                                    ],
                                    [
                                        "firstname" => "Katherine",
                                        "middlename" => null,
                                        "lastname" => "Rosman",
                                        "qualifier" => null,
                                        "title" => null,
                                        "role" => "reported",
                                        "organization" => "",
                                        "rank" => 2
                                    ]
                                ],
                                "organization" => null
                            ],
                            "type_of_material" => "News",
                            "_id" => "nyt://article/dc3c9627-bac3-53cf-8915-d98753e2aea8",
                            "word_count" => 543,
                            "uri" => "nyt://article/dc3c9627-bac3-53cf-8915-d98753e2aea8"
                        ],
                    ],
                    "meta" => [
                        "hits" => 1,
                        "offset" => 10,
                        "time" => 1
                    ]
                ]
            ], Response::HTTP_OK, []),
        ]);
    }
}
