<?php

namespace Tests\Feature\Articles;

use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Services\Integrations\NewsAPIService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class NewsAPITest extends TestCase
{
    public $mockConsoleOutput = false;

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareDatabase();
    }

    /** @test */
    public function will_sync_article_successfully()
    {
        $this->fakeAPIResponse();

        app(NewsAPIService::class)->sync();

        $this->assertEquals(0, Category::count());
        $this->assertEquals(1, Source::count());
        $this->assertEquals(1, Author::count());
        $this->assertEquals(1, Article::count());

        $this->assertEquals('Fox News', Source::first()->name);
        $this->assertEquals('Associated Press', Author::first()->name);
        $this->assertEquals('India, France, UAE agree to take on projects focusing on climate change', Article::first()->title);
    }

    private function fakeAPIResponse()
    {
        Http::fake([
            'https://newsapi.org/*' => Http::response([
                "status" => "ok",
                "totalResults" => 1,
                "articles" => [
                    [
                        "source" => [
                            "id" => "fox-news",
                            "name" => "Fox News"
                        ],
                        "author" => "Associated Press",
                        "title" => "India, France, UAE agree to take on projects focusing on climate change",
                        "description" => "India, France, UAE agreed to take on energy projects with a focus on climate change, biodiversity, and nuclear power. They will also consider key issues like food security and pollution.",
                        "url" => "https://www.foxnews.com/world/india-france-uae-agree-take-projects-focusing-climate-change",
                        "urlToImage" => "https://static.foxnews.com/foxnews.com/content/uploads/2023/01/DOTCOM_STATE_COUNTRY_NEWS_EUROPE-2.png",
                        "publishedAt" => "2023-02-06T23:59:58Z",
                        "content" => "India, France and the United Arab Emirates on Saturday agreed on a trilateral initiative to undertake energy projects with a focus on solar and nuclear sources, fight climate change and protect biodi… [+1746 chars]"
                    ],
                ]
            ], Response::HTTP_OK, []),
        ]);
    }
}
