<?php

namespace Tests\Feature\Articles;

use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Services\Integrations\TheGuardianService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class TheGuardianTest extends TestCase
{
    public $mockConsoleOutput = false;

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareDatabase();
    }

    /** @test */
    public function will_sync_article_successfully()
    {
        $this->fakeAPIResponse();

        app(TheGuardianService::class)->sync();

        $this->assertEquals(1, Category::count());
        $this->assertEquals(1, Source::count());
        $this->assertEquals(0, Author::count());
        $this->assertEquals(1, Article::count());

        $this->assertEquals('UK news', Category::first()->name);
        $this->assertEquals('The Guardian', Source::first()->name);
        $this->assertEquals('Expert diver says he ‘doesn’t think’ Nicola Bulley is in River Wyre', Article::first()->title);
    }

    private function fakeAPIResponse()
    {
        Http::fake([
            'https://content.guardianapis.com/*' => Http::response([
                "response" => [
                    "status" => "ok",
                    "userTier" => "developer",
                    "total" => 1,
                    "startIndex" => 1,
                    "pageSize" => 100,
                    "currentPage" => 1,
                    "pages" => 1,
                    "orderBy" => "newest",
                    "results" => [
                        [
                            "id" => "uk-news/2023/feb/06/nicola-bulley-missing-search-river-forensic-expert",
                            "type" => "article",
                            "sectionId" => "uk-news",
                            "sectionName" => "UK news",
                            "webPublicationDate" => "2023-02-06T23:58:17Z",
                            "webTitle" => "Expert diver says he ‘doesn’t think’ Nicola Bulley is in River Wyre",
                            "webUrl" => "https://www.theguardian.com/uk-news/2023/feb/06/nicola-bulley-missing-search-river-forensic-expert",
                            "apiUrl" => "https://content.guardianapis.com/uk-news/2023/feb/06/nicola-bulley-missing-search-river-forensic-expert",
                            "isHosted" => false,
                            "pillarId" => "pillar/news",
                            "pillarName" => "News"
                        ]
                    ]
                ]
            ], Response::HTTP_OK, []),
        ]);
    }
}
