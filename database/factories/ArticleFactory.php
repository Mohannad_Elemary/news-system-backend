<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => fake()->name(),
            'description' => fake()->name(),
            'date' => fake()->dateTime(),
            'category_id' => Category::factory()->create()->id,
            'author_id' => Author::factory()->create()->id,
            'source_id' => Source::factory()->create()->id,
        ];
    }
}
