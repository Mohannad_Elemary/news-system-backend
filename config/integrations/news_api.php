<?php

return [
    'endpoint' => env('NEWS_API_ENDPOINT', 'https://newsapi.org/v2/everything'),
    'api_key' => env('NEWS_API_API_KEY', 'c4836eec1c2a464fa40cf6dbe9167730'),
];
