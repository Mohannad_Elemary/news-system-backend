<?php

use App\Services\Integrations\NewsAPIService;
use App\Services\Integrations\NewYorkNewsAPIService;
use App\Services\Integrations\TheGuardianService;

return [
    'available_integrations' => [
        'the_guardian' => TheGuardianService::class,
        'news_api' => NewsAPIService::class,
        'newyork_news' => NewYorkNewsAPIService::class,
    ],
];
