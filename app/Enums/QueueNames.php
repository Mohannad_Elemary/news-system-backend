<?php

namespace App\Enums;

abstract class QueueNames
{
    public const SYNC_ARTICLES = 'sync-articles';

    public const SAVE_ARTICLES = 'save-articles';
}
