<?php

namespace App\Enums\Integrations;

abstract class GuardianNewsEnum
{
    // Request params
    public const API_KEY = 'api-key';

    public const FROM_DATE = 'from-date';

    public const TO_DATE = 'to-date';

    public const PAGE = 'page';

    public const PAGE_SIZE = 'page-size';

    // Response params
    public const PAGES = 'pages';

    public const CATEGORY = 'sectionName';

    public const DATE = 'webPublicationDate';

    public const TITLE = 'webTitle';

    public const DESCRIPTION = 'webUrl';

    public const RESPONSE = 'response';

    public const RESULTS = 'results';

    // Defaults
    public const DEFAULT_PAGE_SIZE = 100;

    public const INTEGRATION_NAME = 'The Guardian';
}
