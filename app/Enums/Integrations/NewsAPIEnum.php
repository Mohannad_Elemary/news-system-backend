<?php

namespace App\Enums\Integrations;

abstract class NewsAPIEnum
{
    // Request params
    public const API_KEY = 'apiKey';

    public const FROM_DATE = 'from';

    public const TO_DATE = 'to';

    public const PAGE = 'page';

    public const SORT_BY = 'sortBy';

    public const SEARCH_BY = 'q';

    // Response params
    public const TOTAL_RESULTS = 'totalResults';

    public const SOURCE = 'source';

    public const SOURCE_NAME = 'name';

    public const AUTHOR = 'author';

    public const TITLE = 'title';

    public const DESCRIPTION = 'description';

    public const DATE = 'publishedAt';

    public const ARTICLES = 'articles';

    // Defaults
    public const SEARCH_VALUE = 'a'; // random number as the api always requires it

    public const SORT_VALUE = 'publishedAt';

    public const DEFAULT_PER_PAGE = 100;
}
