<?php

namespace App\Enums\Integrations;

abstract class NewYorkNewsEnum
{
    // Request params
    public const API_KEY = 'api-key';

    public const FROM_DATE = 'begin_date';

    public const TO_DATE = 'end_date';

    public const PAGE = 'page';

    public const SORT_BY = 'sort';

    // Response params
    public const RESPONSE = 'response';

    public const RESPONSE_DOCS = 'docs';

    public const RESPONSE_META = 'meta';

    public const RESPONSE_META_HITS = 'hits';

    public const HEADLINE = 'headline';

    public const HEADLINE_MAIN = 'main';

    public const DESCRIPTION = 'lead_paragraph';

    public const SOURCE = 'source';

    public const DATE = 'pub_date';

    public const CATEGORY = 'section_name';

    public const BYLINE = 'byline';

    public const BYLINE_ORIGINAL = 'original';

    // Defaults
    public const SORT_VALUE = 'newest';

    public const DEFAULT_PER_PAGE = 10;

    public const DEFAULT_SLEEP = 5; // in seconds
}
