<?php

namespace App\Enums;

abstract class AppDefaults
{
    public const PAGE_SIZE = 20;

    public const JOB_TIMEOUT = 200;
}
