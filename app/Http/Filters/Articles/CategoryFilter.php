<?php

namespace App\Http\Filters\Articles;

use App\Http\Filters\Base\Filter as BaseFilter;
use Illuminate\Database\Eloquent\Builder;

class CategoryFilter extends BaseFilter
{
    /**
     * Search with keyword
     *
     * @param  string|null  $value
     * @return Builder
     */
    public function name(string $value = null): Builder
    {
        return $this->builder->fullText(['name'], $value);
    }
}
