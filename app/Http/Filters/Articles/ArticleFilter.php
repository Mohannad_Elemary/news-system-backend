<?php

namespace App\Http\Filters\Articles;

use App\Http\Filters\Base\Filter as BaseFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class ArticleFilter extends BaseFilter
{
    /**
     * Search with keyword
     *
     * @param  string|null  $value
     * @return Builder
     */
    public function keyword(string $value = null): Builder
    {
        return $this->builder->fullText(['title', 'description'], $value);
    }

    /**
     * Filter with starting date
     *
     * @param  string|null  $value
     * @return Builder
     */
    public function dateFrom(string $value = null): Builder
    {
        return $this->builder->where('date', '>=', Carbon::createFromTimestamp($value)->toDateTimeString());
    }

    /**
     * Filter with end date
     *
     * @param  string|null  $value
     * @return Builder
     */
    public function dateTo(string $value = null): Builder
    {
        return $this->builder->where('date', '<=', Carbon::createFromTimestamp($value)->toDateTimeString());
    }

    /**
     * Filter with specific sources
     *
     * @param  array|null  $value
     * @return Builder
     */
    public function sources(array $value = []): Builder
    {
        return $this->builder->whereIn('source_id', $value);
    }

    /**
     * Filter with specific categories
     *
     * @param  array|null  $value
     * @return Builder
     */
    public function categories(array $value = []): Builder
    {
        return $this->builder->whereIn('category_id', $value);
    }

    /**
     * Filter with specific authors
     *
     * @param  array|null  $value
     * @return Builder
     */
    public function authors(array $value = []): Builder
    {
        return $this->builder->whereIn('author_id', $value);
    }
}
