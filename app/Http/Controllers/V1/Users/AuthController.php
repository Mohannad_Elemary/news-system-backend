<?php

namespace App\Http\Controllers\V1\Users;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Base\BaseRequest;
use App\Http\Resources\Base\FailureResource;
use App\Http\Resources\Base\SuccessResource;
use App\Http\Resources\Users\UserResource;
use App\Services\Users\AuthTokenService;
use App\Services\Users\UserService;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Psr\Http\Message\ServerRequestInterface;

class AuthController extends Controller
{
    /**
     * Register user
     *
     * @param  RegisterRequest  $request
     * @param  ServerRequestInterface  $serverRequest
     * @return SuccessResource
     */
    public function register(RegisterRequest $request, ServerRequestInterface $serverRequest): SuccessResource
    {
        $user = app(UserService::class)->store($request->validated());

        $token = $this->generateAndRetrieveToken(
            $request,
            $request->email,
            $request->password,
            $serverRequest
        );

        return new SuccessResource(
            array_merge(
                (new UserResource($user))->toArray($request),
                $token
            ),
            __('general.success'),
            Response::HTTP_OK,
            $token['access_token']
        );
    }

    /**
     * Login user
     *
     * @param  RegisterRequest  $request
     * @param  ServerRequestInterface  $serverRequest
     * @return SuccessResource
     */
    public function login(LoginRequest $request, ServerRequestInterface $serverRequest): SuccessResource
    {
        $token = $this->generateAndRetrieveToken(
            $request,
            $request->email,
            $request->password,
            $serverRequest
        );

        $user = app(UserService::class)->getByEmail($request->email);

        return new SuccessResource(
            array_merge(
                (new UserResource($user))->toArray($request),
                $token
            ),
            __('general.success'),
            Response::HTTP_OK,
            $token['access_token']
        );
    }

    /**
     * Generate an access token to return to the user
     *
     * @param  BaseRequest  $request
     * @param  string  $email
     * @param  string  $password
     * @param  ServerRequestInterface  $serverRequest
     * @return FailureResource|array
     */
    private function generateAndRetrieveToken(BaseRequest $request, string $email, string $password, ServerRequestInterface $serverRequest): FailureResource|array
    {
        $result = app(AuthTokenService::class)->generateToken(
            $request,
            $email,
            $password,
            $serverRequest
        );

        if ($result['statusCode'] != Response::HTTP_OK) {
            return abort(new FailureResource([], __('auth.wrong_credentials'), $result['statusCode']));
        }

        return $result['response'];
    }
}
