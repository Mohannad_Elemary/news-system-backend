<?php

namespace App\Http\Controllers\V1\Users;

use App\Http\Requests\Users\StoreUserSettingsRequest;
use App\Http\Resources\Base\SuccessResource;
use App\Services\Users\UserSettingService;
use Illuminate\Routing\Controller;

class UserSettingController extends Controller
{
    /**
     * Update logged-in user's preferences
     *
     * @param  StoreUserSettingsRequest  $request
     * @return SuccessResource
     */
    public function updateForAuthUser(StoreUserSettingsRequest $request): SuccessResource
    {
        app(UserSettingService::class)->update($request->validated(), auth()->id());

        return new SuccessResource([]);
    }
}
