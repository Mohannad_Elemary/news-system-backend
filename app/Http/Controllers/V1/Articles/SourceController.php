<?php

namespace App\Http\Controllers\V1\Articles;

use App\Services\Articles\SourceService;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller;

class SourceController extends Controller
{
    /**
     * Get sources
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        return app(SourceService::class)->index();
    }
}
