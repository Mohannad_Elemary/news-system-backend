<?php

namespace App\Http\Controllers\V1\Articles;

use App\Services\Articles\CategoryService;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller;

class CategoryController extends Controller
{
    /**
     * Get categories
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        return app(CategoryService::class)->index();
    }
}
