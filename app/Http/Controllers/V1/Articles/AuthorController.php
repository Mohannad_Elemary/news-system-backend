<?php

namespace App\Http\Controllers\V1\Articles;

use App\Services\Articles\AuthorService;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller;

class AuthorController extends Controller
{
    /**
     * Get authors
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        return app(AuthorService::class)->index();
    }
}
