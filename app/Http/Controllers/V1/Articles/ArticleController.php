<?php

namespace App\Http\Controllers\V1\Articles;

use App\Http\Resources\Articles\ArticleResource;
use App\Http\Resources\Base\FailureResource;
use App\Services\Articles\ArticleService;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller;

class ArticleController extends Controller
{
    /**
     * Get articles
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        return app(ArticleService::class)->index();
    }

    /**
     * Show single article
     *
     * @return ArticleResource
     */
    public function show(int $id): ArticleResource
    {
        return new ArticleResource(app(ArticleService::class)->show($id));
    }
}
