<?php

namespace App\Http\Requests\Base;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    protected $label = '';

    /**
     * Construct validation message errors.
     *
     * @return array
     */
    public function messages(): array
    {
        $validationMessages = [];

        if ($this->validations) {
            foreach ($this->validations as $validation) {
                $fileName = strtolower($this->fileName);
                $validationMessages[$validation] = __("$fileName/validations.$this->label.".str_replace('.', ':', $validation));
            }
        }

        return $validationMessages;
    }
}
