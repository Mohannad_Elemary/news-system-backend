<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Base\BaseRequest;

class StoreUserSettingsRequest extends BaseRequest
{
    protected $label = 'user_settings';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sources' => 'array',
            'sources.*' => 'exists:sources,id',
            'categories' => 'array',
            'categories.*' => 'exists:categories,id',
            'authors' => 'array',
            'authors.*' => 'exists:authors,id',
        ];
    }
}
