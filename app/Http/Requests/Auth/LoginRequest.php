<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Base\BaseRequest;

class LoginRequest extends BaseRequest
{
    protected $label = 'login';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string',
        ];
    }
}
