<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Base\BaseRequest;

class RegisterRequest extends BaseRequest
{
    protected $label = 'register';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'email' => 'email|required|unique:users,email',
            'password' => ['confirmed', 'string', 'min:8',
                'regex:/[a-z]/', 'regex:/[A-Z]/', 'regex:/[0-9]/',
                'different:email',
            ],
        ];
    }
}
