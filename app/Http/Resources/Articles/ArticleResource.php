<?php

namespace App\Http\Resources\Articles;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'source' => new SourceResource($this->source),
            'category' => new CategoryResource($this->category),
            'author' => new AuthorResource($this->author),
            'date' => Carbon::parse($this->date)->timestamp,
        ];
    }
}
