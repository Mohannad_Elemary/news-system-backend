<?php

namespace App\Http\Resources\Base;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class SuccessResource extends JsonResource
{
    public static $wrap = false;

    private $message;

    private $code;

    private $accessToken;

    /**
     * FailureResource constructor.
     *
     * @param $resource
     * @param  string  $message
     * @param  int  $code
     */
    public function __construct($resource, $message = '', $code = Response::HTTP_OK, $accessToken = null)
    {
        parent::__construct($resource);
        $this->message = $message ? $message : __('general.success');
        $this->code = $code;
        $this->accessToken = $accessToken;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->resource && is_array($this->resource) && isset($this->resource['data']) ?
            $this->resource : ['data' => $this->resource];

        return array_merge([
            'message' => $this->message,
        ], $data);
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode($this->code);

        if ($this->accessToken) {
            $response->cookie(
                '_token', // key
                $this->accessToken ?? null, // value
                20080, // minutes
                null, // path
                null, // domain
                true, // secure
                true, // httpOnly
                'None', // sameSite
            );
        }
    }
}
