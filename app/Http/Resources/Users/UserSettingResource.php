<?php

namespace App\Http\Resources\Users;

use App\Http\Resources\Articles\AuthorResource;
use App\Http\Resources\Articles\CategoryResource;
use App\Http\Resources\Articles\SourceResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserSettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sources' => SourceResource::collection($this->sources),
            'categories' => CategoryResource::collection($this->categories),
            'authors' => AuthorResource::collection($this->authors),
        ];
    }
}
