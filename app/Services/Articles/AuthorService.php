<?php

namespace App\Services\Articles;

use App\Repositories\Articles\AuthorRepository;
use App\Services\Base\BaseService;

class AuthorService extends BaseService
{
    /**
     * @param  AuthorRepository  $repository
     */
    public function __construct(AuthorRepository $repository)
    {
        parent::__construct($repository);
    }
}
