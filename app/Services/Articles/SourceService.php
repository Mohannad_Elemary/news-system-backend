<?php

namespace App\Services\Articles;

use App\Repositories\Articles\SourceRepository;
use App\Services\Base\BaseService;

class SourceService extends BaseService
{
    /**
     * @param  SourceRepository  $repository
     */
    public function __construct(SourceRepository $repository)
    {
        parent::__construct($repository);
    }
}
