<?php

namespace App\Services\Articles;

use App\Repositories\Articles\CategoryRepository;
use App\Services\Base\BaseService;

class CategoryService extends BaseService
{
    /**
     * @param  CategoryRepository  $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        parent::__construct($repository);
    }
}
