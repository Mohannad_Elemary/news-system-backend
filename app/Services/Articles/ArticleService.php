<?php

namespace App\Services\Articles;

use App\Repositories\Articles\ArticleRepository;
use App\Repositories\Articles\AuthorRepository;
use App\Repositories\Articles\CategoryRepository;
use App\Repositories\Articles\SourceRepository;
use App\Services\Base\BaseService;

class ArticleService extends BaseService
{
    /**
     * List of relations to eager-load in the listing
     *
     *  @var array
     * */
    public $relations = ['author', 'category', 'source'];

    /**
     * @param  ArticleRepository  $repository
     */
    public function __construct(ArticleRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * Save the article's chunk that we synced from the integration
     *
     * @param  array  $data
     * @return void
     */
    public function saveSyncedArticles(array $data): void
    {
        $articlesToSave = [];

        foreach ($data as $article) {
            // Create / update the associated source
            $source = $article['source'] ? app(SourceRepository::class)->updateOrCreate($article['source'], $article['source']) : null;

            // Create / update the associated author
            $author = $article['author'] ? app(AuthorRepository::class)->updateOrCreate($article['author'], $article['author']) : null;

            // Create / update the associated category
            $category = $article['category'] ? app(CategoryRepository::class)->updateOrCreate($article['category'], $article['category']) : null;

            // Add those new IDs to the article data
            $articleToSave = array_merge($article['article'], [
                'source_id' => $source ? $source->id : null,
                'author_id' => $author ? $author->id : null,
                'category_id' => $category ? $category->id : null,
            ]);

            $articlesToSave[] = $articleToSave;
        }

        // Save articles in the DB as bulk (in one query)
        $this->repository->upsert($articlesToSave, array_keys($articlesToSave[0]));
    }
}
