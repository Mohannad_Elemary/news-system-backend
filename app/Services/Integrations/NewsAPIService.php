<?php

namespace App\Services\Integrations;

use App\Adapters\NewsAPIAdapter;
use App\Enums\Integrations\NewsAPIEnum;
use App\Jobs\SaveArticlesJob;
use App\Repositories\Articles\ArticleRepository;
use App\Services\Base\BaseService;
use App\Services\Integrations\Base\BaseIntegrationServiceInterface;
use Illuminate\Support\Facades\Http;

class NewsAPIService extends BaseService implements BaseIntegrationServiceInterface
{
    /**
     * @var NewsAPIAdapter
     */
    public $adapter;

    public function __construct(ArticleRepository $repository, NewsAPIAdapter $adapter)
    {
        parent::__construct($repository);

        $this->adapter = $adapter;
    }

    /**
     * Sync articles from integration
     *
     * @return void
     */
    public function sync(): void
    {
        $currentPage = 1;
        $continueLooping = true;

        // Keep looping as long as there's still pages remaining
        while ($continueLooping) {
            // Send request to fetch the current page's articles (chunk)
            $response = Http::get(
                config('integrations.news_api.endpoint'),
                $this->adapter->formatRequest($currentPage)
            );

            $body = $response->json();
            $data = $body[NewsAPIEnum::ARTICLES];

            // No data received, abort
            if (!$data) {
                $continueLooping = false;
                break;
            }

            // Prepare the synced data to be saved in the database
            $formatedData = $this->adapter->formatResponse($data);

            // Dispatch a job in the queue to save the synced data
            SaveArticlesJob::dispatch($formatedData);

            // Check if there's still pages remaining
            if (ceil($body[NewsAPIEnum::TOTAL_RESULTS] / NewsAPIEnum::DEFAULT_PER_PAGE) >= $currentPage) {
                $continueLooping = false;
                break;
            }

            // temporary condition as News API returns millions of results
            if ($currentPage == 1) {
                $continueLooping = false;
                break;
            }

            $currentPage++;
        }
    }
}
