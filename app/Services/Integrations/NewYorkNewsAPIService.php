<?php

namespace App\Services\Integrations;

use App\Adapters\NewYorkNewsAdapter;
use App\Enums\Integrations\NewYorkNewsEnum;
use App\Jobs\SaveArticlesJob;
use App\Repositories\Articles\ArticleRepository;
use App\Services\Base\BaseService;
use App\Services\Integrations\Base\BaseIntegrationServiceInterface;
use Illuminate\Support\Facades\Http;

class NewYorkNewsAPIService extends BaseService implements BaseIntegrationServiceInterface
{
    /**
     * @var NewYorkNewsAdapter
     */
    public $adapter;

    public function __construct(ArticleRepository $repository, NewYorkNewsAdapter $adapter)
    {
        parent::__construct($repository);

        $this->adapter = $adapter;
    }

    /**
     * Sync articles from integration
     *
     * @return void
     */
    public function sync(): void
    {
        $currentPage = 1;
        $continueLooping = true;

        // Keep looping as long as there's still pages remaining
        while ($continueLooping) {
            // Send request to fetch the current page's articles (chunk)
            $response = Http::get(
                config('integrations.newyork_news.endpoint'),
                $this->adapter->formatRequest($currentPage)
            );

            $body = $response->json();

            // No data received, abort
            if (!isset($body[NewYorkNewsEnum::RESPONSE])) {
                $continueLooping = false;
                break;
            }

            $data = $body[NewYorkNewsEnum::RESPONSE][NewYorkNewsEnum::RESPONSE_DOCS];

            // No data received, abort
            if (!$data) {
                $continueLooping = false;
                break;
            }

            // Prepare the synced data to be saved in the database
            $formatedData = $this->adapter->formatResponse($data);

            // Dispatch a job in the queue to save the synced data
            SaveArticlesJob::dispatch($formatedData);

            // Check if there's still pages remaining
            if ((int)floor($body[NewYorkNewsEnum::RESPONSE][NewYorkNewsEnum::RESPONSE_META][NewYorkNewsEnum::RESPONSE_META_HITS]) === $currentPage) {
                $continueLooping = false;
                break;
            }

            $currentPage++;

            // Newyork API has an aggressive rate limiter, so we need to reduce the number of calls ber minute
            sleep(NewYorkNewsEnum::DEFAULT_SLEEP);
        }
    }
}
