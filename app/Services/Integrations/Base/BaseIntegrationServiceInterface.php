<?php

namespace App\Services\Integrations\Base;

interface BaseIntegrationServiceInterface
{
    public function sync(): void;
}
