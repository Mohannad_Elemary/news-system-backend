<?php

namespace App\Services\Integrations;

use App\Adapters\TheGuardianAdapter;
use App\Enums\Integrations\GuardianNewsEnum;
use App\Jobs\SaveArticlesJob;
use App\Repositories\Articles\ArticleRepository;
use App\Services\Base\BaseService;
use App\Services\Integrations\Base\BaseIntegrationServiceInterface;
use Illuminate\Support\Facades\Http;

class TheGuardianService extends BaseService implements BaseIntegrationServiceInterface
{
    /**
     * @var TheGuardianAdapter
     */
    public $adapter;

    public function __construct(ArticleRepository $repository, TheGuardianAdapter $adapter)
    {
        parent::__construct($repository);

        $this->adapter = $adapter;
    }

    /**
     * Sync articles from integration
     *
     * @return void
     */
    public function sync(): void
    {
        $currentPage = 1;
        $continueLooping = true;

        // Keep looping as long as there's still pages remaining
        while ($continueLooping) {
            // Send request to fetch the current page's articles (chunk)
            $response = Http::get(
                config('integrations.the_guardian.endpoint'),
                $this->adapter->formatRequest($currentPage)
            );

            $body = $response->json();
            $data = $body[GuardianNewsEnum::RESPONSE][GuardianNewsEnum::RESULTS];

            // No data received, abort
            if (!$data) {
                $continueLooping = false;
                break;
            }

            // Prepare the synced data to be saved in the database
            $formatedData = $this->adapter->formatResponse($data);

            // Dispatch a job in the queue to save the synced data
            SaveArticlesJob::dispatch($formatedData);

            // Check if there's still pages remaining
            if ($body[GuardianNewsEnum::RESPONSE][GuardianNewsEnum::PAGES] === $currentPage) {
                $continueLooping = false;
                break;
            }

            $currentPage++;
        }
    }
}
