<?php

namespace App\Services\Users;

use App\Repositories\Users\UserRepository;
use App\Services\Base\BaseService;
use Illuminate\Database\Eloquent\Model;

class UserService extends BaseService
{
    /**
     * @param  UserRepository  $repository
     */
    public function __construct(UserRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * Get user by email
     *
     * @param  string  $email
     * @return Model
     */
    public function getByEmail(string $email): Model
    {
        return $this->repository->getByEmail($email);
    }
}
