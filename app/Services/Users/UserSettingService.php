<?php

namespace App\Services\Users;

use App\Repositories\Users\UserSettingRepository;
use App\Services\Base\BaseService;

class UserSettingService extends BaseService
{
    /**
     * @param  UserSettingRepository  $repository
     */
    public function __construct(UserSettingRepository $repository)
    {
        parent::__construct($repository);
    }
}
