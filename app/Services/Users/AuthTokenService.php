<?php

namespace App\Services\Users;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Passport\Http\Controllers\AccessTokenController;

class AuthTokenService
{
    /**
     * Send request to auth server to retrieve user token
     *
     * @param $body
     * @return array
     */
    public function sendTokenRequest($serverRequest, $body): array
    {
        $request = $serverRequest->withParsedBody($body);

        try {
            $response = app(AccessTokenController::class)->issueToken($request);
        } catch (\Exception $e) {
            return ['statusCode' => Response::HTTP_BAD_REQUEST];
        }

        $data['statusCode'] = $response->getStatusCode();
        $data['response'] = json_decode((string) $response->getContent(), true);

        return $data;
    }

    /**
     * Prepare request body to generate token
     *
     * @param  Request  $request
     * @param $email
     * @param $password
     * @return array
     */
    public function prepareTokenApiBody($request, $email, $password): array
    {
        return  [
            'grant_type' => 'password',
            'client_id' => $request['client_id'],
            'client_secret' => $request['client_secret'],
            'username' => $email,
            'password' => $password,
            'scope' => '*',
        ];
    }

    /**
     * @param  Request  $request
     * @param $email
     * @param $password
     * @return mixed
     */
    public function generateToken($request, $email, $password, $serverRequest): array
    {
        return $this->sendTokenRequest(
            $serverRequest,
            $this->prepareTokenApiBody($request, $email, $password)
        );
    }
}
