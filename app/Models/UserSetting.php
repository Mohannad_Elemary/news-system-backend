<?php

namespace App\Models;

use App\Repositories\Articles\AuthorRepository;
use App\Repositories\Articles\CategoryRepository;
use App\Repositories\Articles\SourceRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'user_id',
        'sources',
        'categories',
        'authors',
    ];

    /**
     * Get preferred sources of specific user
     *
     * @param  string  $value
     * @return Collection
     */
    public function getSourcesAttribute(string $value): Collection
    {
        $sources = json_decode($value);

        return app(SourceRepository::class)->fetchByIDs($sources);
    }

    /**
     * Get preferred categories of specific user
     *
     * @param  string  $value
     * @return Collection
     */
    public function getCategoriesAttribute(string $value): Collection
    {
        $categories = json_decode($value);

        return app(CategoryRepository::class)->fetchByIDs($categories);
    }

    /**
     * Get preferred authors of specific user
     *
     * @param  string  $value
     * @return Collection
     */
    public function getAuthorsAttribute(string $value): Collection
    {
        $authors = json_decode($value);

        return app(AuthorRepository::class)->fetchByIDs($authors);
    }

    /**
     * Set preferred sources of specific user
     *
     * @param  array  $value
     * @return Collection
     */
    public function setSourcesAttribute(array $value): void
    {
        $this->attributes['sources'] = json_encode($value ?? []);
    }

    /**
     * Set preferred categories of specific user
     *
     * @param  array  $value
     * @return Collection
     */
    public function setCategoriesAttribute(array $value): void
    {
        $this->attributes['categories'] = json_encode($value ?? []);
    }

    /**
     * Set preferred authors of specific user
     *
     * @param  array  $value
     * @return Collection
     */
    public function setAuthorsAttribute(array $value): void
    {
        $this->attributes['authors'] = json_encode($value ?? []);
    }
}
