<?php

namespace App\Repositories\Articles;

use App\Enums\AppDefaults;
use App\Http\Filters\Articles\SourceFilter;
use App\Http\Resources\Articles\AuthorResource;
use App\Models\Source;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SourceRepository extends BaseRepository
{
    protected $pagination = AppDefaults::PAGE_SIZE;

    /**
     * Define repository's model
     *
     * @return string
     */
    public function model(): string
    {
        return Source::class;
    }

    /**
     * Return list of sources
     *
     * @return ResourceCollection
     */
    public function indexResource(): ResourceCollection
    {
        return AuthorResource::collection($this->getModelData(app(SourceFilter::class)));
    }

    /**
     * Fetch sources by IDs
     *
     * @param  array  $ids
     * @return Collection
     */
    public function fetchByIDs(array $ids): Collection
    {
        return $this->model->whereIn('id', $ids)->get();
    }
}
