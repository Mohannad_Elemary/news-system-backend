<?php

namespace App\Repositories\Articles;

use App\Enums\AppDefaults;
use App\Http\Filters\Articles\ArticleFilter;
use App\Http\Resources\Articles\ArticleResource;
use App\Models\Article;
use App\Repositories\Base\BaseRepository;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ArticleRepository extends BaseRepository
{
    protected $pagination = AppDefaults::PAGE_SIZE;

    /**
     * Define repository's model
     *
     * @return string
     */
    public function model(): string
    {
        return Article::class;
    }

    /**
     * Return list of articles
     *
     * @return ResourceCollection
     */
    public function indexResource(): ResourceCollection
    {
        return ArticleResource::collection($this->getModelData(app(ArticleFilter::class)));
    }
}
