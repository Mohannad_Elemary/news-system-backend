<?php

namespace App\Repositories\Articles;

use App\Enums\AppDefaults;
use App\Http\Filters\Articles\AuthorFilter;
use App\Http\Resources\Articles\SourceResource;
use App\Models\Author;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AuthorRepository extends BaseRepository
{
    protected $pagination = AppDefaults::PAGE_SIZE;

    /**
     * Define repository's model
     *
     * @return string
     */
    public function model(): string
    {
        return Author::class;
    }

    /**
     * Return list of authors
     *
     * @return ResourceCollection
     */
    public function indexResource(): ResourceCollection
    {
        return SourceResource::collection($this->getModelData(app(AuthorFilter::class)));
    }

    /**
     * Fetch authors by IDs
     *
     * @param  array  $ids
     * @return Collection
     */
    public function fetchByIDs(array $ids): Collection
    {
        return $this->model->whereIn('id', $ids)->get();
    }
}
