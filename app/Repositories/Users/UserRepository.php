<?php

namespace App\Repositories\Users;

use App\Models\User;
use App\Repositories\Base\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserRepository extends BaseRepository
{
    /**
     * Define repository's model
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Return list of sources
     *
     * @return ResourceCollection
     */
    public function indexResource(): ResourceCollection
    {
        return $this->resource::collection($this->getModelData());
    }

    /**
     * Get user by email
     *
     * @return Model
     */
    public function getByEmail(string $email): Model
    {
        return $this->where(['email' => $email])->first();
    }
}
