<?php

namespace App\Jobs;

use App\Enums\QueueNames;
use App\Services\Articles\ArticleService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveArticlesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Articles to save.
     *
     * @var array
     */
    public $articles;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $articles)
    {
        $this->articles = $articles;

        $this->onQueue(QueueNames::SAVE_ARTICLES);
    }

    /**
     * Save a chunk of articles we fetched from the integration
     *
     * @return void
     */
    public function handle()
    {
        app(ArticleService::class)->saveSyncedArticles($this->articles);
    }
}
