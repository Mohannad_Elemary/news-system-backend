<?php

namespace App\Jobs;

use App\Enums\AppDefaults;
use App\Enums\QueueNames;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncArticlesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Integration Service.
     *
     * @var string
     */
    public $integrationService;

    public $timeout = AppDefaults::JOB_TIMEOUT;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $integrationService)
    {
        $this->integrationService = $integrationService;

        $this->onQueue(QueueNames::SYNC_ARTICLES);
    }

    /**
     * Sync articles from specific integration
     *
     * @return void
     */
    public function handle()
    {
        app($this->integrationService)->sync();
    }
}
