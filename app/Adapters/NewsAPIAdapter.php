<?php

namespace App\Adapters;

use App\Adapters\Base\BaseAdapterInterface;
use App\Enums\Integrations\NewsAPIEnum;
use Carbon\Carbon;

class NewsAPIAdapter implements BaseAdapterInterface
{
    /**
     * Format the parameters to send to the request
     *
     * @param  int  $page
     * @return array
     */
    public function formatRequest(int $page): array
    {
        return [
            NewsAPIEnum::API_KEY => config('integrations.news_api.api_key'),
            NewsAPIEnum::FROM_DATE => Carbon::yesterday()->format('Y-m-d'),
            NewsAPIEnum::TO_DATE => Carbon::yesterday()->format('Y-m-d'),
            NewsAPIEnum::PAGE => $page,
            NewsAPIEnum::SEARCH_BY => NewsAPIEnum::SEARCH_VALUE,
            NewsAPIEnum::SORT_BY => NewsAPIEnum::SORT_VALUE,
        ];
    }

    /**
     * Format the request's response to prepare to save to the database
     *
     * @param  array  $data
     * @return array
     */
    public function formatResponse(array $data): array
    {
        $formatedData = [];

        foreach ($data as $article) {
            $formatedData[] = [
                'article' => [
                    'title' => $article[NewsAPIEnum::TITLE],
                    'description' => $article[NewsAPIEnum::DESCRIPTION],
                    'date' => Carbon::parse($article[NewsAPIEnum::DATE])->toDateTimeString(),
                ],
                'source' => [
                    'name' => $article[NewsAPIEnum::SOURCE][NewsAPIEnum::SOURCE_NAME],
                ],
                'category' => null,
                'author' => $article[NewsAPIEnum::AUTHOR] ? [
                    'name' => $article[NewsAPIEnum::AUTHOR],
                ] : null,
            ];
        }

        return $formatedData;
    }
}
