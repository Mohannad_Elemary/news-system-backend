<?php

namespace App\Adapters;

use App\Enums\Integrations\GuardianNewsEnum;
use Carbon\Carbon;

class TheGuardianAdapter
{
    /**
     * Format the parameters to send to the request
     *
     * @param  int  $page
     * @return array
     */
    public function formatRequest(int $page)
    {
        return [
            GuardianNewsEnum::API_KEY => config('integrations.the_guardian.api_key'),
            GuardianNewsEnum::FROM_DATE => Carbon::yesterday()->format('Y-m-d'),
            GuardianNewsEnum::TO_DATE => Carbon::yesterday()->format('Y-m-d'),
            GuardianNewsEnum::PAGE => $page,
            GuardianNewsEnum::PAGE_SIZE => GuardianNewsEnum::DEFAULT_PAGE_SIZE,
        ];
    }

    /**
     * Format the request's response to prepare to save to the database
     *
     * @param  array  $data
     * @return array
     */
    public function formatResponse(array $data)
    {
        $formatedData = [];

        foreach ($data as $article) {
            $formatedData[] = [
                'article' => [
                    'title' => $article[GuardianNewsEnum::TITLE],
                    'description' => $article[GuardianNewsEnum::DESCRIPTION],
                    'date' => Carbon::parse($article[GuardianNewsEnum::DATE])->toDateTimeString(),
                ],
                'source' => [
                    'name' => GuardianNewsEnum::INTEGRATION_NAME,
                ],
                'category' => [
                    'name' => $article[GuardianNewsEnum::CATEGORY],
                ],
                'author' => null,
            ];
        }

        return $formatedData;
    }
}
