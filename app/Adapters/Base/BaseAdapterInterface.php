<?php

namespace App\Adapters\Base;

interface BaseAdapterInterface
{
    /**
     * Format the parameters to send to the request
     *
     * @param  int  $page
     * @return array
     */
    public function formatRequest(int $page): array;

    /**
     * Format the request's response to prepare to save to the database
     *
     * @param  array  $data
     * @return array
     */
    public function formatResponse(array $data): array;
}
