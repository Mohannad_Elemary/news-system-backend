<?php

namespace App\Adapters;

use App\Enums\Integrations\NewYorkNewsEnum;
use Carbon\Carbon;

class NewYorkNewsAdapter
{
    /**
     * Format the parameters to send to the request
     *
     * @param  int  $page
     * @return array
     */
    public function formatRequest(int $page)
    {
        return [
            NewYorkNewsEnum::API_KEY => config('integrations.newyork_news.api_key'),
            NewYorkNewsEnum::FROM_DATE => str_replace('-', '', Carbon::yesterday()->format('Y-m-d')),
            NewYorkNewsEnum::TO_DATE => str_replace('-', '', Carbon::yesterday()->format('Y-m-d')),
            NewYorkNewsEnum::PAGE => $page,
            NewYorkNewsEnum::SORT_BY => NewYorkNewsEnum::SORT_VALUE,
        ];
    }

    /**
     * Format the request's response to prepare to save to the database
     *
     * @param  array  $data
     * @return array
     */
    public function formatResponse(array $data)
    {
        $formatedData = [];

        foreach ($data as $article) {
            $formatedData[] = [
                'article' => [
                    'title' => $article[NewYorkNewsEnum::HEADLINE][NewYorkNewsEnum::HEADLINE_MAIN],
                    'description' => $article[NewYorkNewsEnum::DESCRIPTION],
                    'date' => Carbon::parse($article[NewYorkNewsEnum::DATE])->toDateTimeString(),
                ],
                'source' => [
                    'name' => $article[NewYorkNewsEnum::SOURCE],
                ],
                'category' => isset($article[NewYorkNewsEnum::CATEGORY]) ? [
                    'name' => $article[NewYorkNewsEnum::CATEGORY],
                ] : null,
                'author' => $article[NewYorkNewsEnum::BYLINE][NewYorkNewsEnum::BYLINE_ORIGINAL] ? [
                    'name' => $article[NewYorkNewsEnum::BYLINE][NewYorkNewsEnum::BYLINE_ORIGINAL] ? substr($article[NewYorkNewsEnum::BYLINE][NewYorkNewsEnum::BYLINE_ORIGINAL], 3) : null,
                ] : null,
            ];
        }

        return $formatedData;
    }
}
