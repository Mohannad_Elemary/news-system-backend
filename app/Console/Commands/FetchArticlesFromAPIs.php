<?php

namespace App\Console\Commands;

use App\Jobs\SyncArticlesJob;
use Illuminate\Console\Command;

class FetchArticlesFromAPIs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch articles from external APIs';

    /**
     * Fetch the articles from news integrations everyday
     *
     * @return int
     */
    public function handle()
    {
        // For each available integration, dispatch a job to the queue to start
        // fetching its data
        foreach (config('integrations.integrations.available_integrations') as $integration) {
            SyncArticlesJob::dispatch($integration);
        }

        return Command::SUCCESS;
    }
}
